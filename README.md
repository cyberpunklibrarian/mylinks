# MyLinks

Honestly, I have no problem with Linktr.ee. It's one of those things where I have to wonder why people are paying for something that can be so easily done on your own without directing fans to someone else's website. While Linktr.ee has a free tier, and provides a lightweight site, the person accessing your links is going to a site that's not yours, which means your website isn't the primary URL in their history.

If you're a musician, artist, or anyone with your own website; that's where you should be directing your fans and customers. This little bit of code allows you to do just that. There are no trackers and no JavaScript. This is a mobile first design that's low-weight and consists of nothing more than two images and two text files.

Customize to whatever you like and bring your fans to you, not some third party website.

---

![Screenshot](https://cyberpunklibrarian.com/links/MyLinks.png)